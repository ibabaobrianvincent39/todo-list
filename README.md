# 2nd Onboarding Task - Structured Todo-list Application using Quasar Framework

## Project Setup

### Clone the project

```sh
git clone https://gitlab.com/ibabaobrianvincent39/todo-list.git
```

### Open file directory

```sh
cd todo-list-main
```

### Install project dependencies

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npx quasar dev
```
